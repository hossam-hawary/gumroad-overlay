# README #


*  This repo is pretty simple, you can download the complete repo and open the `overlay/index.html` on a browser to test it, or you can simply check the `overlay/index.js` file
* At the beginning of the js file, I define a list of targeted domains `('gum', 'gumroad', ...)` and if you added more to this list the js logic will work just fine for them too, so, I think this way we can support more custom subdomains and domains for creators

* The `pivot.rb` file includes the same answer for the problem-solving challenge and with some test-cases
