def get_pivot(list = [])
  list_length = list.length
  return -1 if list_length < 3

  head = 1
  tail = list_length - 2
  total_left = list.first
  total_right = list.last
  loop do
    return head if head == tail && total_left == total_right
    return -1 if head == tail && total_left != total_right

    if total_right > total_left
      total_left += list[head]
      head += 1
    else
      total_right += list[tail]
      tail -= 1
    end
  end
end

get_pivot([1, 2, 1]) #    1
get_pivot([-1, 2, -1]) #  1
get_pivot([0, 0, 0, 0, 0]) # 1
get_pivot([1, 2, 0, 1]) # 1
get_pivot([1, 4, 6, 2, 3]) # 2
get_pivot([1, 4, 6, 2, 2, 9, 0, 0, 0]) # 3
get_pivot([1, 4, 6, 2, 2, 2, 9, 0, 0, 0]) # -1
get_pivot([]) # -1
get_pivot([1, 1]) # -1
