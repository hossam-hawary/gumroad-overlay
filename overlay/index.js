
document.addEventListener("DOMContentLoaded", function (event) {
  const targetedDomains = ['gum', 'gumroad', 'sahil'];

  const iframeStyle = {
    border: "none",
    width: "100%",
    height: "100%"
  }

  const shouldReplaceThisLink = function (link) {
    return link.hostname.split('.').some(domain => targetedDomains.includes(domain))
  }

  const replaceWithInlineIframe = function (event) {
    const targetElement = event.target
    event.preventDefault()
    const iframe = document.createElement('iframe');
    Object.entries(iframeStyle).forEach(([key, value]) => { iframe.style[key] = value})
    iframe.src = targetElement.href
    targetElement.replaceWith(iframe);
  }

  // scan all links of the document
  for (const link of document.links) {
    if (shouldReplaceThisLink(link)) {
      link.onclick = replaceWithInlineIframe
    }
  }
});
